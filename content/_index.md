---
header:
  - type: text
    height: 200
    paddingX: 50
    paddingY: 0
    align: center
    title:
      - Hydro-Blogger
    subtitle:
      - Saswata Nandi's Hydrology Blog
    titleColor: white
    titleShadow: true
    titleFontSize: 44
    subtitleColor: 
    subtitleCursive: true
    subtitleFontSize: 18
    spaceBetweenTitleSubtitle: 16
  
  - type: img
    imageSrc: images/header/background.jpg
    imageSize: cover
    imageRepeat: no-repeat
    imagePosition: center
    height: 300
    paddingX: 100
    paddingY: 0
    align: center
    title:
      - Hydrology Blogs 
    subtitle:
      - by Saswata Nandi
    titleColor: white
    titleShadow: false
    titleFontSize: 70
    subtitleColor: MediumAquamarine                      
    subtitleCursive: false
    subtitleFontSize: 30
    spaceBetweenTitleSubtitle: 10

  - type: slide
    height: 500
    slide:
      - paddingX: 50
        paddingY: 0
        align: left
        imageSrc: images/header/background.jpg
        imageSize: cover
        imageRepeat: no-repeat
        imagePosition: center
        title:
          - Hydro-Blogger
        subtitle:
          - Saswata Nandi's Hydrology Blog
        titleColor: white
        subtitleFontSize: 16
        spaceBetweenTitleSubtitle: 20

      - paddingX: 50
        paddingY: 0
        align: center
        imageSrc: images/header/background.jpg
        imageSize: cover
        imageRepeat: no-repeat
        imagePosition: center
        title:
          - header title2
        subtitle:
          - header subtitle2
        titleFontSize: 44
        subtitleFontSize: 16
        spaceBetweenTitleSubtitle: 20

      - paddingX: 50
        paddingY: 0
        align: right
        imageSrc: images/header/background.jpg
        imageSize: cover
        imageRepeat: no-repeat
        imagePosition: center
        title:
          - header title3
        subtitle:
          - header subtitle3
        titleFontSize: 44
        subtitleFontSize: 16
        spaceBetweenTitleSubtitle: 20
---
